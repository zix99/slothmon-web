#!/usr/bin/env nodejs
var http = require("http");
var express = require("express");
var logger = require("morgan");
var fs = require("fs");
var config = require("./config");

console.log("Init...");
var app = express();
var server = http.createServer(app);
app.use(logger("combined"));
app.set("views", __dirname + "/views");


console.log("Enabling minification...");
var minify = require("express-minify");
var cachedir = __dirname + "/cache";
if (!fs.existsSync(cachedir)) fs.mkdirSync(cachedir);

express.static.mime.define({
	'text/less' : ['less']
});

console.log("Setting up browserify...");
var browserify = require("browserify-middleware");
app.use("/static/scripts", browserify("./static/scripts"));


app.use('/static', minify({
	cache: cachedir
}));
app.use('/static', express.static(__dirname + "/static"));


console.log("Setting up view engine...");
var hbs = require("express-handlebars");
app.engine("hbs", hbs({}));
app.set("view engine", "hbs");
app.locals.layout = "default.hbs";


console.log("Adding routes...");
var controllerPath = __dirname + "/controllers/";
fs.readdirSync(controllerPath).forEach(function(file){
	console.log("  %s", file);
	var path = controllerPath + file.substr(0, file.indexOf("."));
	require(path)(app);
});

console.log("Starting http...");
var appserve = server.listen(config.port, function(){
	var lhost = server.address().address;
	var lport = server.address().port;

	console.log("Server listening on http://%s:%s", lhost, lport);
});


console.log("Setting up sockets...");
var io = require('socket.io')(server);
var socketPath = __dirname + "/sockets/";
fs.readdirSync(socketPath).forEach(function(file){
	var path = socketPath + file.substr(0, file.indexOf("."));
	console.log("  %s", file);
	require(path)(io);
});