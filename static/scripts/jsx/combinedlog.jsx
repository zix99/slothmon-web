var React = require("react");
var io = require("socket.io-client");
var config = require("../config.js");
var Log = require("./pages/partials/log.jsx");

module.exports = React.createClass({
	getDefaultProps: function(){
		return {
			log: "Undefined"
		};
	},

	componentDidMount: function(){
		var client = this._client = io.connect(config.baseUri + "/logs");

		client.on("log", function(data){
			this.refs.log.addMessage(data.remote || "Ukn", data.id, data.msg);
		}.bind(this));

		client.emit("sub", this.props.log);
	},

	componentWillUnmount: function(){
		this._client.emit("unsub", this.props.log);
		this._client.removeAllListeners("log");
	},

	render: function(){
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-lg-12">
                        <h1 className="page-header">
                            Combined <small>{this.props.log} Overview</small>
                        </h1>
                    </div>
				</div>
				<div className="row">
					<div className="col-lg-12">
						<Log ref="log" title={this.props.log} />
					</div>
				</div>
			</div>
			);
	}
});