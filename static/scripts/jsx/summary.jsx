var React = require("react");
var io = require("socket.io-client");
var config = require("../config.js");

var KBtoMB = function(m){
	return Math.round(m/1024/1024 * 100) / 100.0;
}

var toPercent = function(d){
	return Math.round(d * 100) + "%";
}

module.exports = React.createClass({
	getInitialState: function(){
		return {
			remotes: {}
		};
	},

	componentDidMount: function(){
		var client = this._client = io.connect(config.baseUri + "/summary");
		client.on("summary", function(data){
			this.state.remotes = data;
			this.forceUpdate();
		}.bind(this));
	},

	componentWillUnmount: function(){
		this._client.removeAllListeners("summary");
	},

	render: function(){

		var remotePartials = [];
		for (var key in this.state.remotes){
			var remote = this.state.remotes[key];
			var summary = remote.summary;

			if (summary)
			{
				var timeSince = Math.round((Date.now() - remote.lastData)/1000); //not technically correct, but meh right now

				var iconClass = "fa fa-fw fa-exclamation";
				var rowClass = "danger";
				if (timeSince < 10){
					rowClass="success";
					iconClass="fa fa-fw fa-check"
				} else if (timeSince < 30){
					rowClass="warning";
					iconClass="fa fa-fw fa-question";
				}

				var usedMemRatio = (summary.totalmem - summary.freemem) / summary.totalmem;

				remotePartials.push(
					<tr key={key} className={rowClass}>
						<td><i className={iconClass}></i></td>
						<td><b>{key}</b></td>
						<td>{summary.platform} {summary.release}</td>
						<td>{KBtoMB(summary.totalmem - summary.freemem)} / {KBtoMB(summary.totalmem)} MB ({toPercent(usedMemRatio)})</td>
						<td>{summary.loadavg.map(function(d){return toPercent(d);}).join(", ")}</td>
						<td>{timeSince} sec</td>
					</tr>
					);
			}
			else
			{
				remotePartials.push(
					<tr key={key} className="warning">
						<td><i className="fa fa-fw fa-question"></i></td>
						<td><b>{key}</b></td>
					</tr>
					);
			}
		}

		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-lg-12">
                        <h1 className="page-header">
                            Summary <small>Node Overview</small>
                        </h1>
                    </div>
				</div>
				<div className="row">
					<div className="col-lg-12">
						<table className="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Status</th>
									<th>Node</th>
									<th>Platform</th>
									<th>Memory</th>
									<th>Load Average</th>
									<th>Last Message</th>
								</tr>
							</thead>
							<tbody>
								{remotePartials}
							</tbody>
						</table>
					</div>
				</div>
			</div>
			);
	}
});