var React = require("react");

var NavSubItem = React.createClass({
	_click: function(){
		if (this.props.onClick)
			this.props.onClick();
	},

	render: function(){
		var classes = "fa fa-fw fa-" + this.props.icon;
		return (
			<li>
				<a href="#" onClick={this._click}><i className={classes}></i> {this.props.name}</a>
			</li>
			);
	}
})

module.exports = React.createClass({
	getInitialState: function(){
		return {
			items: {}
		};
	},

	getDefaultProps: function(){
		return {
			remote: null
		};
	},

	_itemClick: function(metric){
		if (this.props.onClick)
			this.props.onClick(metric);
	},

	render: function(){
		var remote = this.props.remote;
		var subitems = [];
		for (var subKey in this.props.metrics){
			var metric = this.props.metrics[subKey];
			subitems.push(this._buildControl(metric.type, subKey, metric.opts));
		}

		return (<li>
					<a href="#" data-toggle="collapse" data-target={"#pm_" + remote}><i className="fa fa-fw fa-cloud"></i> {remote}</a>
					<ul id={"pm_" + remote} className="collapse">
						{subitems}
					</ul>
				</li>
		);
	},

	_buildControl: function(type, key, opts){
		switch(type.toLowerCase()){
			case "system":
				return <NavSubItem key={key} name="System" icon="cogs" onClick={this._itemClick.bind(this, key)} />
			case "log":
				return <NavSubItem key={key} name={key} icon="terminal" onClick={this._itemClick.bind(this, key)} />
		}
	},
});