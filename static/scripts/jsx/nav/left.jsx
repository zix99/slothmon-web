var React = require("react");
var io = require("socket.io-client");
var config = require("../../config.js");
var LeftNode = require("./leftnode.jsx");
var Remote = require("../pages/remote.jsx");
var Log = require("../pages/log.jsx");
var Dashboard = require("../summary.jsx");
var CombinedLog = require("../combinedlog.jsx");

module.exports = React.createClass({
	getInitialState: function(){
		var socket = io.connect(config.baseUri + "/remotes");
		socket.on("remotes", function(data){
			console.log(data);
			this.state.remotes = data;
			this.forceUpdate();
		}.bind(this) );

		var logsocket = io.connect(config.baseUri + "/logs");
		logsocket.on("combinedlog", function(data){
			this.state.logs = Object.keys(data);
			this.forceUpdate();
		}.bind(this));

		return {
			remotes: [],
			logs: []
		};
	},

	clickDashboard: function(){
		var partial = <Dashboard />;

		if (this.props.changePage && partial)
			this.props.changePage(partial);
	},

	handleClick: function(remote, metric){
		var partial;
		var opts = this.state.remotes[remote][metric];
		var uid = Date.now();
		console.log(opts.type);

		switch(opts.type){
			case "system":
				partial = <Remote key={remote + uid} remote={remote} />
				break;
			case "log":
				partial = <Log key={remote + uid} remote={remote} metric={metric} />
				break;
		}

		if (this.props.changePage && partial)
			this.props.changePage(partial);
	},

	clickLog: function(log){
		var partial = <CombinedLog key={"combined_" + log} log={log} />

		if (this.props.changePage)
			this.props.changePage(partial);
	},

	render: function(){
		return (
			<div className="collapse navbar-collapse navbar-ex1-collapse">
                <ul className="nav navbar-nav side-nav">
                    <li>
                        <a href="#" onClick={this.clickDashboard}><i className="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    
                    {Object.keys(this.state.remotes).map(function(remote){
                    	return <LeftNode key={remote} remote={remote} metrics={this.state.remotes[remote]} onClick={this.handleClick.bind(this, remote)} />;
                    }.bind(this) )}

                    <li>
                    	<a href="#" data-toggle="collapse" data-target="#combinedlogs"><i className="fa fa-fw fa-list"></i> Combined Logs</a>
                    	<ul id="combinedlogs" className="collapse">
		                    {this.state.logs.map(function(log){
		                    	return (
		                    		<li>
		                    			<a href="#" onClick={this.clickLog.bind(this, log)}><i className="fa fa-fw fa-terminal"></i> {log}</a>
		                    		</li>
		                    		);
		                    }.bind(this))};
	                    </ul>
                    </li>

                </ul>
            </div>
			);
	},
});