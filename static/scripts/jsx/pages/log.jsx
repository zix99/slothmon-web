var React = require("react");
var Log = require("./partials/log.jsx");
var config = require("../../config.js");
var io = require("socket.io-client");

module.exports = React.createClass({

	componentDidMount: function(){
		var client = this._client = io.connect(config.baseUri + "/agent");
		
		client.on("log", function(data){
			console.log(data);
			if (data.id === this.props.metric)
				this.refs.log.addMessage(this.props.remote, data.id, data.msg);
		}.bind(this));

		client.emit("sub", this.props.remote);
	},

	componentWillUnmount: function(){
		console.log("Log Leaving %s", this.props.remote)
		this._client.emit('unsub', this.props.remote);
		this._client.removeAllListeners("log");
	},

	render: function(){
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-lg-12">
                        <h1 className="page-header">
                            {this.props.remote} <small>{this.props.metric} Log</small>
                        </h1>
                    </div>
				</div>
				<div className="row">
					<div className="col-lg-12">
						<Log ref="log" title="Log" />
					</div>
				</div>
			</div>
			);
	}
});