var React = require("react");
var io = require("socket.io-client");
var config = require("../../config.js");
var Grapher = require("./partials/scrollinggraph.jsx");

module.exports = React.createClass({
	_sumCycles: function(cpus){
		var used = 0;
		var idle = 0;
		for (var i=0; i<cpus.length; i++)
		{
			var cpu = cpus[i];
			used += cpu.times.user + cpu.times.nice + cpu.times.sys + cpu.times.irq;
			idle += cpu.times.idle;
		}
		return [used, idle];
	},

	_cpuPercent: function(cpus){
		var percent = 0;
		var sums = this._sumCycles(cpus);
		if (this._lastCpu)
		{
			var diffUsed = sums[0] - this._lastCpu[0];
			var diffFree = sums[1] - this._lastCpu[1];
			percent = diffUsed * 100.0 / (diffFree + diffUsed);
		}
		this._lastCpu = sums;
		return percent;
	},

	componentDidUpdate: function(){
		this._lastCpu = null;

		this._client.emit('unsub:all');
		this._client.emit('sub', this.props.remote);

		console.log("Remote updated...");
	},

	componentDidMount: function(){
		this._lastCpu = null;

		var client = this._client = io.connect(config.baseUri + "/agent");
		client.on("stats", function(data){
			this.refs.cpu.addData(this._cpuPercent(data.cpus));

			var totalMem = data.memory.total/1024/1024/1024;
			if (totalMem !== this.refs.ram.state.height)
			{
				this.refs.ram.setState({"height": totalMem});
			}
			this.refs.ram.addData( (data.memory.total - data.memory.free) / 1024.0 / 1024/ 1024 );
		}.bind(this));

		console.log("Joining %s", this.props.remote);
		client.emit("sub", this.props.remote);
	},

	componentWillUnmount: function(){
		console.log("Leaving %s", this.props.remote)
		this._client.emit('unsub', this.props.remote);
		this._client.removeAllListeners("stats");
	},

	render: function() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-lg-12">
                        <h1 className="page-header">
                            {this.props.remote} <small>Server Overview</small>
                        </h1>
                    </div>
				</div>
				<div className="row">
					<div className="col-md-6">
                        <Grapher ref="cpu" title="CPU Usage" />
                    </div>
                    <div className="col-md-6">
                    	<Grapher ref="ram" title="Ram Usage" height="10" />
                    </div>
				</div>
			</div>
			);
	}
});