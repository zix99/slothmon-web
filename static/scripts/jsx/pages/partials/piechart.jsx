var React = require("react");

module.exports = React.createClass({
	getDefaultProps: function(){
		return {
			width: 50,
			height: 110,
			title : "Untitled",
			series: {
				"Undefined 1" : 1,
				"Undefiend 2" : 2
			}
		};
	},

	_mapSeriesToData: function(series){
		var data = [];
		for (var item in series){
			data.push({label: item, data: series[item]});
		}
		return data;
	},

	_setupGraph: function(){
	    var data = this._mapSeriesToData(this.props.series);

	    var node = $(this.refs.pie.getDOMNode());
	    this._plot = $.plot(node, data, {
	        series: {
	            pie: {
	                show: true
	            }
	        },
	        grid: {
	            hoverable: true
	        },
	        tooltip: true,
	        tooltipOpts: {
	            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
	            shifts: {
	                x: 20,
	                y: 0
	            },
	            defaultTheme: false
	        }
	    });
	},

	//This doesn't work right now
	setData: function(data){
		var series = this._mapSeriesToData(data);
		this._plot.setData(series);
		this._plot.processed = false;
		this._plot.draw();
	},

	componentDidUpdate: function(){
		this._setupGraph();
	},

	componentDidMount: function(){
		this._setupGraph();
	},

	render: function(){
		return (
			<div className="panel panel-green">
	            <div className="panel-heading">
	                <h3 className="panel-title"><i className="fa fa-long-arrow-right"></i> {this.props.title}</h3>
	            </div>
	            <div className="panel-body">
	                <div className="flot-chart">
	                    <div className="flot-chart-content" ref="pie"></div>
	                </div>
	            </div>
	        </div>
        );
	}
});