var React = require("react");

module.exports = React.createClass({
	getInitialState: function(){
		return {
			width: 100,
			height: 110
		};
	},

	getDefaultProps: function(){
		return {
			title : "Untitled"
		};
	},

	_setupGraph: function(){
		console.log("Graph setup...");
		this._data = [];

		var opts = {
			grid: {
		        borderWidth: 1,
		        minBorderMargin: 20,
		        labelMargin: 10,
		        backgroundColor: {
		            colors: ["#fff", "#e4f4f4"]
		        },
		        margin: {
		            top: 8,
		            bottom: 20,
		            left: 20
		        }
		    },
		    xaxis: {
		        tickFormatter: function() {
		            return "";
		        }
		    },
		    yaxis: {
		        min: 0,
		        max: this.state.height
		    },
		    legend: {
		        show: true
		    }
		};

		var series = [{
			data: [],
			lines: {
				fill: true
			}
		}];
		for (var i=0; i<this.state.width; i++){
			series[0].data.push([i, 0]);
			this._data.push(0);
		}

		var node = $(this.refs.graph.getDOMNode());
		this._plot = $.plot(node, series, opts);
	},

	addData: function(val){
		this._data = this._data.slice(1);
		this._data.push(val);

		var series = [{
			data: [],
			lines: {
				fill: true
			}
		}];
		for (var i=0; i<this._data.length; i++){
			series[0].data.push([i, this._data[i]]);
		}
		this._plot.setData(series);
		this._plot.draw();
	},

	componentDidUpdate: function(){
		this._setupGraph();
	},

	componentDidMount: function(){
		this._setupGraph();
	},

	render: function(){
		return (
			<div className="panel panel-red">
                <div className="panel-heading">
                    <h3 className="panel-title"><i className="fa fa-long-arrow-right"></i> {this.props.title}</h3>
                </div>
                <div className="panel-body">
                    <div className="flot-chart">
                        <div className="flot-chart-content" ref="graph"></div>
                    </div>
                </div>
            </div>
			);
	}
});