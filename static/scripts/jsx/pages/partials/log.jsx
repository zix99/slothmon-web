var React = require("react");

module.exports = React.createClass({
	getDefaultProps: function(){
		return {
			title: "Untitled",
		};
	},
	getInitialState: function(){
		return {
			text: []
		};
	},

	_updateScroll: function(){
		var ele = this.refs.panel.getDOMNode();
		ele.scrollTop = ele.scrollHeight;
	},

	addMessage: function(remote, ident, msg){
		this.state.text.push(<div>
			<span className="remote">{remote}</span>
			<span className="ident">{ident}</span>
			<span className="message">{msg}</span>
		</div>);
		this.forceUpdate();
		this._updateScroll();
	},

	render: function(){
		return (
			<div className="panel panel-green">
                <div className="panel-heading">
                    <h3 className="panel-title"><i className="fa fa-long-arrow-right"></i> {this.props.title}</h3>
                </div>
                <div className="panel-body">
                	<div ref="panel" className="logpanel">
                		{this.state.text}
                	</div>
                </div>
            </div>
			);
	}
});