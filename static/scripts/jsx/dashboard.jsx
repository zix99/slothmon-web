var React = require("react");
var LeftNav = require("./nav/left.jsx");
var TopNav = require("./nav/top.jsx");
var Summary = require("./summary.jsx");

module.exports = React.createClass({
	getInitialState: function(){
		this.currentPage = <Summary />;
		return {};
	},
	handleChangePage: function(page)
	{
		this.currentPage = page;
		this.forceUpdate();
	},
	render: function(){
		return (
			<div id="wrapper">
				<nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
					<TopNav />
					<LeftNav changePage={this.handleChangePage} />
				</nav>
				<div id="page-wrapper">
					{this.currentPage}
				</div>

			</div>
			);
	}
});