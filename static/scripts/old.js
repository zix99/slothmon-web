var io = require("socket.io-client");
var chart = require("./thirdparty/chart.js");

var leftnav = require("./jsx/nav.jsx");

var data = {
    labels: [],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(0,220,0,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
        }/*,
        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
        }*/
    ]
};

var opts = {
	animate: false,
	bezierCurve: false,
	scaleOverride: true,
	scaleBeginAtZero: true,
	scaleStartValue: 0,
	scaleStepWidth: 10,
	scaleSteps: 10
};

var pidata = [
    {
        value: 300,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Used"
    },
    {
        value: 50,
        color: "#FFF",
        highlight: "#FFF",
        label: "Free"
    }
];

(function(){
	var ctx = document.getElementById("test").getContext("2d");
	var c = new chart(ctx).Line(data, opts);
	var m = new chart(document.getElementById("mem").getContext("2d")).Pie(pidata);
	c.addData([0], "");

	var lastCpus = null;

	var socket = io.connect();
	socket.on("hi", function(data){
		console.log("Server said hi");
	});
	socket.on("stats", function(data){
		console.log(data);
		//var usedmem = (data.memory.total - data.memory.free) / 1024 /1024;
		//c.addData([usedmem], "Merp");

		if (lastCpus)
		{
			var cpuUsed = 0;
			var cpuIdle = 0;

			for (var i=0; i<data.cpus.length; i++)
			{
				var cpu = data.cpus[i];
				var last = lastCpus[i];
				cpuUsed += (cpu.times.irq + cpu.times.nice + cpu.times.sys + cpu.times.user) - (last.times.irq + last.times.nice + last.times.sys + last.times.user);
				cpuIdle += (cpu.times.idle - last.times.idle);
			}

			var p = ~~((cpuUsed / (cpuUsed + cpuIdle)) * 100);
			c.addData([p], "");
			if (c.datasets[0].points.length > 20)
				c.removeData();
		}
		lastCpus = data.cpus;

		var SCALE = 1024 * 1024;
		m.segments[0].value = (data.memory.total - data.memory.free) / SCALE;
		m.segments[1].value = (data.memory.free) / SCALE;
		m.update();

	});
})();
