var args = require("minimist")(process.argv.slice(2));

module.exports = {
	prod : args.prod || false,
	port : args.port || 23020,

	remotes: {
		"Local" : "127.0.0.1:23021",
		"TV" : "tv.lan:23021",
		"Node1" : "node1.zdyn.net:23021",
	},
};