var ioclient = require("socket.io-client");
var config = require("../config");

module.exports = function(io){
	var nsp = io.of("/remotes");

	var remotes = {};
	var clients = [];

	for (var key in config.remotes){
		remotes[key] = [];

		(function(remote)
		{
			var client = ioclient.connect("http://" + config.remotes[key]);
			client.on("state", function(data){
				remotes[remote] = data;
				console.log("STATE %s : %s", remote, JSON.stringify(data));

				for (var i=0; i<clients.length; i++)
					clients[i].emit("remotes", remotes);
			}.bind(this));
		})(key);
	}

	nsp.on('connection', function(socket){
		socket.emit("remotes", remotes);
		clients.push(socket);

		socket.on("disconnect", function(){
			clients.splice(clients.indexOf(socket),1);
		});
	});
};