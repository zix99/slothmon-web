var config = require("../config");
var ioclient = require("socket.io-client");

module.exports = function(io){
	var nsp = io.of("/summary");

	var summary = {};

	for (var remote in config.remotes)
	{
		summary[remote] = {};

		console.log("Adding summary remote %s...", remote);
		var client = ioclient.connect("http://" + config.remotes[remote]);
		
		(function setup(dst){
			client.on("summary", function(data){
				summary[dst] = {
					lastData : Date.now(),
					summary: data
				};
			});
		})(remote);
	}

	nsp.on('connection', function(socket){
		var timer = setInterval(function(){
			socket.emit("summary", summary);
		}, 2500);
		socket.on("disconnect", function(){
			clearInterval(timer);
		});
	});

};