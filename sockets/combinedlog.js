var config = require("../config");
var ioclient = require("socket.io-client");

module.exports = function(io){
	var nsp = io.of("/logs");

	var clients = {};
	var logs = {};

	for (var remote in config.remotes)
	{
		console.log("Adding remote %s...", remote);
		var client = clients[remote] = ioclient.connect("http://" + config.remotes[remote]);
		
		(function setup(dst){
			client.on("state", function(data){
				for (var metricKey in data){
					var metric = data[metricKey];
					if (metric.type === "log"){
						logs[metricKey] = true;
					}
				}

				nsp.emit("combinedlog", logs);
			});

			client.on("log", function(data){
				data.remote = dst;
				nsp.to(data.id).emit("log", data);
			});
		})(remote);
	}

	nsp.on('connection', function(socket){
		nsp.emit("combinedlog", logs);

		socket.on('sub', function(data){
			console.log("Joining log %s", data);
			socket.join(data);
		});

		socket.on('unsub', function(data){
			socket.leave(data);
		});

		socket.on('unsub:all', function(){
			for (var remote in config.remotes)
			{
				socket.leave(remote);
			}
		});
	});

};