var config = require("../config");
var ioclient = require("socket.io-client");

module.exports = function(io){
	var nsp = io.of("/agent");

	var clients = {};

	for (var remote in config.remotes)
	{
		console.log("Adding remote %s...", remote);
		var client = clients[remote] = ioclient.connect("http://" + config.remotes[remote]);
		
		(function setup(dst){
			client.on("system", function(data){
				nsp.to(dst).emit("stats", data);
			});
			client.on("log", function(data){
				nsp.to(dst).emit("log", data);
			});
		})(remote);
	}

	nsp.on('connection', function(socket){
		socket.emit("hi");

		socket.on('sub', function(data){
			console.log("Joining %s", data);
			socket.join(data);
		});

		socket.on('unsub', function(data){
			socket.leave(data);
		});

		socket.on('unsub:all', function(){
			for (var remote in config.remotes)
			{
				socket.leave(remote);
			}
		});
	});

};