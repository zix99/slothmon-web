# Sloth Monitor Web

This is the web-ui for the sltoh monitoring agent.

## Setup

Simply configure the instance in config.js, and then run:

 * npm install
 * node index.js